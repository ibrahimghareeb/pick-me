import 'package:flutter/material.dart';


class TextFieldName extends StatelessWidget {
  late double leftMargin;
  late double topMargin;
  late double rightMargin;
  late double bottomMargin;
  late final TextEditingController controller;
  late TextInputType textInputType;
  late double fontSizeAll;
  late Color colorFontStyle;
  late String hintText;
  late double hintSize;
  late Color hintColor;
  late String labelText;
  late double labelSize;
  late Color labelColor;
  late double errorSize;
  late Color errorColor;
  late double borderRadius;
  late double borderWidth;
  late Color borderColor;
  late double enabledBorderRadius;
  late double enabledBorderWidth;
  late Color enabledBorderColor;
  late double focusedBorderRadius;
  late double focusedBorderWidth;
  late Color focusedBorderColor;
  late double errorBorderRadius;
  late double errorBorderWidth;
  late Color errorBorderColor;
  late final String? Function(String?)? validate;
  TextFieldName({required this.leftMargin,required this.topMargin,required this.rightMargin,required this.bottomMargin,required this.controller,required this.textInputType,required this.fontSizeAll,required this.colorFontStyle,required this.hintText,required this.hintSize,required this.hintColor,required this.labelText,required this.labelSize,required this.labelColor,required this.errorSize,required this.errorColor,required this.borderRadius,required this.borderWidth,required this.borderColor,required this.enabledBorderRadius,required this.enabledBorderWidth,required this.enabledBorderColor,required this.focusedBorderRadius,required this.focusedBorderWidth,required this.focusedBorderColor,required this.errorBorderRadius,required this.errorBorderWidth,required this.errorBorderColor,required this.validate});






  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(leftMargin, topMargin, rightMargin, bottomMargin),
      child: TextFormField(

        controller: controller,
        keyboardType: textInputType,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: fontSizeAll,
          color: colorFontStyle,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 5),
          hintText: hintText,
          hintStyle: TextStyle(
            fontSize: hintSize,
            color: hintColor,
          ),
          labelText: labelText,
          labelStyle: TextStyle(
            fontSize: labelSize,
            color: labelColor,
          ),
          errorStyle: TextStyle(
              fontSize: errorSize,
              color: errorColor
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
              borderSide: BorderSide(
                width: borderWidth,
                color: borderColor,
              )
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(enabledBorderRadius)),
              borderSide: BorderSide(
                width: enabledBorderWidth,
                color: enabledBorderColor,
              )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(focusedBorderRadius)),
              borderSide: BorderSide(
                width: focusedBorderWidth,
                color: focusedBorderColor,
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(errorBorderRadius)),
              borderSide: BorderSide(
                width: errorBorderWidth,
                color: errorBorderColor,
              )
          ),
        ),
        validator: validate,

      ),
    );
  }
}


class TextFieldPassword extends StatefulWidget {
  late double leftMargin;
  late double topMargin;
  late double rightMargin;
  late double bottomMargin;
  late final TextEditingController controller;
  late TextInputType textInputType;
  late double fontSizeAll;
  late Color colorFontStyle;
  late String hintText;
  late double hintSize;
  late Color hintColor;
  late String labelText;
  late double labelSize;
  late Color labelColor;
  late double errorSize;
  late Color errorColor;
  late double borderRadius;
  late double borderWidth;
  late Color borderColor;
  late double enabledBorderRadius;
  late double enabledBorderWidth;
  late Color enabledBorderColor;
  late double focusedBorderRadius;
  late double focusedBorderWidth;
  late Color focusedBorderColor;
  late double errorBorderRadius;
  late double errorBorderWidth;
  late Color errorBorderColor;
  late final String? Function(String?)? validate;
  TextFieldPassword({required this.leftMargin,required this.topMargin,required this.rightMargin,required this.bottomMargin,required this.controller,required this.textInputType,required this.fontSizeAll,required this.colorFontStyle,required this.hintText,required this.hintSize,required this.hintColor,required this.labelText,required this.labelSize,required this.labelColor,required this.errorSize,required this.errorColor,required this.borderRadius,required this.borderWidth,required this.borderColor,required this.enabledBorderRadius,required this.enabledBorderWidth,required this.enabledBorderColor,required this.focusedBorderRadius,required this.focusedBorderWidth,required this.focusedBorderColor,required this.errorBorderRadius,required this.errorBorderWidth,required this.errorBorderColor,required this.validate});

  @override
  State<TextFieldPassword> createState() => _TextFieldPasswordState();
}

class _TextFieldPasswordState extends State<TextFieldPassword> {
  late bool obscure=true;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.fromLTRB(widget.leftMargin, widget.topMargin, widget.rightMargin, widget.bottomMargin),
      child: TextFormField(
        obscureText: obscure,

        controller: widget.controller,
        keyboardType: widget.textInputType,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: widget.fontSizeAll,
          color: widget.colorFontStyle,
        ),
        decoration: InputDecoration(
          suffixIcon: IconButton(icon: Icon(obscure?Icons.remove_red_eye:Icons.remove,size: 20,color: Color(0xFF303030),),onPressed: (){
            setState(() {
              obscure=!obscure;
            });
          },),
          contentPadding: EdgeInsets.only(left: 5),


          hintText: widget.hintText,
          hintStyle: TextStyle(
            fontSize: widget.hintSize,
            color: widget.hintColor,
          ),
          labelText: widget.labelText,
          labelStyle: TextStyle(
            fontSize: widget.labelSize,
            color: widget.labelColor,
          ),
          errorStyle: TextStyle(
              fontSize: widget.errorSize,
              color: widget.errorColor
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.borderRadius)),
              borderSide: BorderSide(
                width: widget.borderWidth,
                color: widget.borderColor,
              )
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.enabledBorderRadius)),
              borderSide: BorderSide(
                width: widget.enabledBorderWidth,
                color: widget.enabledBorderColor,
              )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.focusedBorderRadius)),
              borderSide: BorderSide(
                width: widget.focusedBorderWidth,
                color: widget.focusedBorderColor,
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.errorBorderRadius)),
              borderSide: BorderSide(
                width: widget.errorBorderWidth,
                color: widget.errorBorderColor,
              )
          ),
        ),
        validator: widget.validate,

      ),
    );
  }
}
