import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pick_me/pages/show_trip_map.dart';


class DetailWidget extends StatefulWidget {
  late String advertiserName;
  late String dateAndTime;
  late String numberOfPeople;
  late String descriptionText;
  late final GestureTapCallback onPressed;
  late String dropdownButtonValue ='one';
  late List<String> listOfNamesDropdownButton=['one','tow'];
  late final LatLng sourceMap;
  late final LatLng destinationMap;

  DetailWidget(
      {required this.advertiserName,
        required this.dateAndTime,
        required this.numberOfPeople,
        required this.descriptionText,
        required this.onPressed,
        required this.sourceMap,
        required this.destinationMap});

  @override
  State<DetailWidget> createState() => _DetailWidgetState();
}

class _DetailWidgetState extends State<DetailWidget> {
  late String drValue = 'One';
  late bool show = true;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  show = !show;
                });
              },
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.15,
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.03,
                    left: MediaQuery.of(context).size.width * 0.02),
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.02),
                decoration: BoxDecoration(
                    color: const Color.fromRGBO(250, 200, 102, 150),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                    border: Border.all(
                      width: 2,
                      color: const Color(0xFF5c5c3d),
                    )),
                child: Column(
                  children: [
                    Row(
                      //  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: [
                        const Text(
                          'Advertiser name :',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.08,
                        ),
                        Text(
                          widget.advertiserName,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Row(
                      children: [
                        const Text(
                          'Date and Time :',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.08,
                        ),
                        Text(
                          widget.dateAndTime,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            show
                ? Container()
                : Container(
              width: MediaQuery.of(context).size.width * 0.9,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01),
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * 0.04),
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(250, 200, 102, 150),
                  border:
                  Border.all(width: 2, color: const Color(0xFF5c5c3d)),
                  borderRadius: const BorderRadius.all(Radius.circular(12))),
              child: Column(
                children: [
                  // here Container for map
                  //TODO: FOR MAP
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.4,
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.02),
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 2, color: const Color(0xFF5c5c3d)),
                        borderRadius:
                        const BorderRadius.all(Radius.circular(12))),
                    //TODO: HERE IS THE MAP
                    child: ShowTripMap(source: widget.sourceMap, destination: widget.destinationMap) ,
                  ),
                  // here Container for real data
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.4,
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.04,
                        left: MediaQuery.of(context).size.width * 0.01,
                        right: MediaQuery.of(context).size.width * 0.01),
                    decoration: BoxDecoration(
                        border:
                        Border.all(width: 2, color: Color(0xFF5c5c3d)),
                        borderRadius:
                        const BorderRadius.all(Radius.circular(12))),
                    child: Column(
                      children: [
                        // here first row for number of people
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                'Number Of People : ',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.2,
                                height:
                                MediaQuery.of(context).size.height * 0.04,
                                decoration: BoxDecoration(
                                    color: const Color.fromRGBO(
                                        214, 214, 194, 100),
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF5c5c3d)),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10))),
                                //TODO: HERE for text number of people
                                child: Center(
                                    child: Text(
                                      widget.numberOfPeople,
                                      style: const TextStyle(
                                          color: Colors.black, fontSize: 20),
                                    )),
                              )
                            ]),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        // here is the name of people DropdownButton
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                'People Names : ',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                width:
                                MediaQuery.of(context).size.width * 0.085,
                              ),
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.2,
                                height:
                                MediaQuery.of(context).size.height * 0.04,
                                decoration: BoxDecoration(
                                    color: const Color.fromRGBO(
                                        214, 214, 194, 100),
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF5c5c3d)),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10))),
                                //TODO: HERE for name of people dropdown button
                                child: DropdownButton<String>(
                                  value: widget.dropdownButtonValue,
                                  hint: const Text("Names"),
                                  icon: const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.black,
                                    size: 20,
                                  ),
                                  iconSize: 10,
                                  isExpanded: true,
                                  underline: Container(),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      drValue = newValue!;
                                    });
                                  },
                                  items: widget.listOfNamesDropdownButton
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        //here for description
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'Description : ',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height:
                              MediaQuery.of(context).size.height * 0.2,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(
                                      214, 214, 194, 100),
                                  border: Border.all(
                                      width: 2,
                                      color: const Color(0xFF5c5c3d)),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10))),
                              child: Center(
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Text(
                                      widget.descriptionText,
                                      style: const TextStyle(
                                          color: Colors.black, fontSize: 20),
                                    ),
                                  )),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  MaterialButton(
                    onPressed: widget.onPressed,
                    height: MediaQuery.of(context).size.height*0.08,
                    minWidth: MediaQuery.of(context).size.width*0.4,
                    color: const Color(0xFFd7af0f),
                    shape: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(
                        color: Color(0xFF5c5c3d),
                        width: 2,
                      ),
                    ),
                    child: const Text(
                      'Pick Me',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                ],
              ),
            ),
            // Container of detail
          ],
        ));
  }
}
