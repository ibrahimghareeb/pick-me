// import 'package:flutter/material.dart';
// import 'package:pick_me/models/connection_manger.dart';
// import 'package:pick_me/models/response.dart';
// import 'package:pick_me/models/trip_model.dart';
// import 'package:pick_me/models/user_model.dart';
//
// class TestModel extends StatefulWidget {
//   const TestModel({Key? key}) : super(key: key);
//
//   @override
//   _TestModelState createState() => _TestModelState();
// }
//
// class _TestModelState extends State<TestModel> {
//   late List<TripModel> tripinfo = [];
//   late List<UserModel> users = [];
//
//   void getData() async {
//     try {
//       setState(() {});
//
//       Response? result = await getTripFormApi();
//
//       if (result!.statusCode == 200) {
//         tripinfo = result.body;
//         // ignore: avoid_print
//         print(tripinfo.length);
//         tripinfo.forEach((element) {
//           print(element.username);
//         });
//       }
//       setState(() {});
//     } catch (e) {
//       setState(() {
//         print(e);
//       });
//     }
//   }
//
//   void log() async {
//     try {
//       Response? response = await logIn("ibrahim", "bero1234");
//       print(response!.statusCode);
//     } catch (e) {
//       print(e);
//     }
//   }
//
//   void addTr() async {
//     try {
//       Response? response = await addTrip("zain", 4, "2021/12/2", "11:00",
//           "description", 35.525262, 35.797189, 33.526212, 36.226783);
//       print(response!.statusCode);
//     } catch (e) {
//       print(e);
//     }
//   }
//
//   void req() async {
//     try {
//       Response? response = await reqToJoin("ibrahim", "3");
//       print(response!.statusCode);
//     } catch (e) {
//       print(e);
//     }
//   }
//
//   void getUsers() async {
//     try {
//       setState(() {});
//
//       Response? result = await showTripPassengers("3");
//
//       if (result!.statusCode == 200) {
//         users = result.body;
//         // ignore: avoid_print
//         print(users.length);
//       }
//       setState(() {});
//     } catch (e) {
//       setState(() {
//         print(e);
//       });
//     }
//   }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//   }
//
//   @override
//   void didChangeDependencies() {
//     // TODO: implement didChangeDependencies
//     super.didChangeDependencies();
//     // getData();
//     // log();
//     // addTr();
//     // req();
//     getUsers();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.blue,
//       ),
//     );
//   }
// }
