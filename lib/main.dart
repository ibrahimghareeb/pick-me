import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pick_me/pages/launcher.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // Here for Screen rotation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        backgroundColor: Colors.white,
      ),
      home: const LauncherPage(),
      // Here removed debug from screen
      debugShowCheckedModeBanner: false,
    );
  }
}




