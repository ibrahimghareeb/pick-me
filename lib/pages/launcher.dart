import 'package:flutter/material.dart';
import 'package:pick_me/pages/login.dart';



class LauncherPage extends StatefulWidget {
  const LauncherPage({Key? key}) : super(key: key);

  @override
  _LauncherPageState createState() => _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(const Duration(seconds: 7)).then((value) =>
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const LoginPage())));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width*1,
        height: MediaQuery.of(context).size.height*1,
        decoration: const BoxDecoration(
          color:  Color(0xFF303030),
        ),
        child: Center(
          child: Container(
            width:MediaQuery.of(context).size.width*0.5 ,
            height: MediaQuery.of(context).size.height*0.5,

            decoration: const BoxDecoration(
                color: Color(0xFF303030),
                image: DecorationImage(
                  image: AssetImage('assets/images/Logo.png'),
                  fit: BoxFit.contain,)

            ),
          ),
        ),

      ),
    );
  }
}
