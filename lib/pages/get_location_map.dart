import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class GetLocationMap extends StatefulWidget {
  const GetLocationMap({Key? key}) : super(key: key);

  @override
  _GetLocationMapState createState() => _GetLocationMapState();
}

class _GetLocationMapState extends State<GetLocationMap> {
  List<Marker> myMarker = [];
  LatLng? markerLocation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            child: const Text(
              'OK',
              style: TextStyle(color: Colors.black),
            ),
            backgroundColor: Colors.yellow,
            splashColor: Colors.yellowAccent,
            onPressed: () {
              Navigator.pop(context, markerLocation);
            }),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Stack(
          children: [
            GoogleMap(
              initialCameraPosition: const CameraPosition(
                target: LatLng(35.542578354244476, 35.78535385472284),
                zoom: 14,
              ),
              onTap: _handleTap,
              myLocationButtonEnabled: true,
              markers: Set.from(myMarker),
              mapType: MapType.hybrid,
              zoomControlsEnabled: false,
              myLocationEnabled: true,
            ),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Container(
                child: const Text(
                  'Pick Me',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.yellow,
                  ),
                ),
                alignment: Alignment.topCenter,
              ),
            ),
          ],
        ));
  }

  _handleTap(LatLng tappedPoint) {
    markerLocation = tappedPoint;
    setState(() {
      myMarker = [];
      myMarker.add(
        Marker(
          markerId: MarkerId(tappedPoint.toString()),
          position: tappedPoint,
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow),
        ),
      );
    });
  }
}
