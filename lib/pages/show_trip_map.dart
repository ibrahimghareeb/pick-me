import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ShowTripMap extends StatefulWidget {
  const ShowTripMap({Key? key, required this.source, required this.destination})
      : super(key: key);

  final LatLng source;
  final LatLng destination;

  @override
  _SecondGoogleMapPageState createState() => _SecondGoogleMapPageState();
}

class _SecondGoogleMapPageState extends State<ShowTripMap> {
  Set<Marker> myMarker = {};

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myMarker.add(Marker(
        markerId: const MarkerId('source'),
        position: widget.source,
        icon:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen)));
    myMarker.add(Marker(
        markerId: const MarkerId('destination'),
        position: widget.destination,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: widget.source,
          zoom: 14,
        ),
        markers: myMarker,
        mapType: MapType.hybrid,
        zoomControlsEnabled: true,
      ),
    );
  }
}
