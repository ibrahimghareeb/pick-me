import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:pick_me/models/connection_manger.dart';
import 'package:pick_me/models/response.dart';
import 'package:pick_me/pages/get_location_map.dart';


class Announcing_Page extends StatelessWidget {
  const Announcing_Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Announcing_body(),
    );
  }
}

class Announcing_body extends StatefulWidget {
  const Announcing_body({Key? key}) : super(key: key);

  @override
  _Announcing_bodyState createState() => _Announcing_bodyState();
}

class _Announcing_bodyState extends State<Announcing_body> {
  late LatLng sourceAnnounce = LatLng(0, 0);
  late LatLng destinationAnnounce = LatLng(0, 0);
  late DateTime date = DateTime.now();
  late TimeOfDay time;
  late TextEditingController description =TextEditingController();
  late TimeOfDay picked;

  late int _currentValue = 1;

  datepicker() {
    showDatePicker(
            helpText: "Date",
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2021),
            builder: (context, child) => Theme(
                data: ThemeData().copyWith(
                  colorScheme: ColorScheme.dark(
                    primary: Color(0xFFd7af0f),
                    onPrimary: Colors.yellow,
                    surface: Color(0xFFd7af0f),
                    onSurface: Colors.black,
                  ),
                  dialogBackgroundColor: Colors.grey.shade400,
                ),
                child: child!),
            lastDate: DateTime(2024))
        .then((value) {
      if (value == null) {
        return;
      }
      setState(() {
        date = value;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    time = TimeOfDay.now();
  }

  Future<Null> selecttime(BuildContext context) async {
    picked = (await showTimePicker(
      context: context,
      initialTime: time,
      builder: (context, child) => Theme(
          data: ThemeData().copyWith(
            colorScheme: ColorScheme.highContrastDark(
              primary: Color(0xFFd7af0f),
              onBackground: Colors.yellow,
              onPrimary: Colors.yellowAccent,
              surface: Colors.grey,
              onSurface: Colors.black,
            ),
          ),
          child: child!),
    ))!;
    if (picked != null) {
      setState(() {
        time = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          image:DecorationImage(
              image: AssetImage('assets/images/app_background.jpg'),
              fit: BoxFit.cover),
        ),
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.13,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.83,
              decoration: BoxDecoration(
                color: Color.fromRGBO(250, 200, 102, 150),
                border: Border.all(
                  color: Color(0xFF5c5c3d),
                  width: 2,
                ),
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, top: 15,bottom: 15),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //from
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(214, 214, 194, 100),
                              border:
                                  Border.all(color: Color(0xFF5c5c3d), width: 2),
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  sourceAnnounce.toString(),
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          MaterialButton(
                            onPressed: () async {
                              var result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const GetLocationMap()));
                              setState(() {
                                sourceAnnounce = result;
                              });
                            },
                            height: MediaQuery.of(context).size.height*0.06,
                            minWidth: MediaQuery.of(context).size.width*0.12,
                            color: Color(0xFFd7af0f),
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                color: Color(0xFF5c5c3d),
                                width: 2,
                              ),
                            ),
                            child: Text(
                              'From',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                      //here for to
                      Padding(
                        padding:  EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height: MediaQuery.of(context).size.height * 0.05,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(214, 214, 194, 100),
                                border: Border.all(
                                    color: Color(0xFF5c5c3d), width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Text(
                                    destinationAnnounce.toString(),
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            MaterialButton(
                              onPressed: () async {
                                var result = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const GetLocationMap()));
                                setState(() {
                                  destinationAnnounce = result;
                                });
                              },
                              height: MediaQuery.of(context).size.height*0.06,
                              minWidth: MediaQuery.of(context).size.width*0.19,
                              color: Color(0xFFd7af0f),
                              shape: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  color: Color(0xFF5c5c3d),
                                  width: 2,
                                ),
                              ),
                              child: Text(
                                'To',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //Date
                      Padding(
                        padding:  EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.06),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.7,
                              height: MediaQuery.of(context).size.height * 0.05,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(214, 214, 194, 100),
                                border: Border.all(
                                    color: Color(0xFF5c5c3d), width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: Text(
                                  (DateFormat.yMMMd().format(date)).toString(),
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                                onPressed: () {
                                  datepicker();
                                },
                                icon: Icon(
                                  Icons.date_range,
                                  size: 30,
                                )),
                          ],
                        ),
                      ),
                      //Time
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.7,
                              height: MediaQuery.of(context).size.height * 0.05,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(214, 214, 194, 100),
                                border: Border.all(
                                    color: Color(0xFF5c5c3d), width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: Text(
                                  time.format(context).toString(),
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                                onPressed: () {
                                  selecttime(context);
                                },
                                icon: Icon(
                                  Icons.alarm,
                                  size: 30,
                                )),
                          ],
                        ),
                      ),
                      //number of people
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'number of people :',
                              style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.1,
                              height: MediaQuery.of(context).size.height * 0.05,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(214, 214, 194, 100),
                                border: Border.all(
                                    color: Color(0xFF5c5c3d), width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: Text(
                                  '$_currentValue',
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.09,
                              height: MediaQuery.of(context).size.height * 0.085,
                              // width: 40,
                              // height: 60,
                              decoration: BoxDecoration(
                                color: Color(0xFFd7af0f),
                                border: Border.all(
                                    color: Color(0xFF5c5c3d), width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                              ),
                              child: NumberPicker(
                                value: _currentValue,
                                itemHeight: MediaQuery.of(context).size.height*0.0318,
                                itemWidth: MediaQuery.of(context).size.width*0.0318,
                                minValue: 1,
                                maxValue: 5,
                                textStyle: TextStyle(color: Colors.grey.shade600),
                                selectedTextStyle: TextStyle(color: Colors.black),
                                onChanged: (value) =>
                                    setState(() => _currentValue = value),
                                decoration: BoxDecoration(
                                  // borderRadius: BorderRadius.circular(16),
                                  border: Border.all(color: Colors.black26),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //description
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                        child: TextFormField(
                          controller: description,
                          minLines: 5,
                          maxLines: 5,
                          cursorColor: Color(0xFFd7af0f),
                          keyboardType: TextInputType.multiline,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Color.fromRGBO(214, 214, 194, 100),
                            hintText: 'Descripton',
                            hintStyle: TextStyle(
                              color: Colors.grey.shade700,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              borderSide:
                                  BorderSide(color: Color(0xFF5c5c3d), width: 2),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              borderSide:
                                  BorderSide(color: Color(0xFF5c5c3d), width: 2),
                            ),
                          ),
                        ),
                      ),
                      //submit
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
                        child: Center(
                          child: MaterialButton(
                            onPressed: () async {
                              Response? resopnse=await addTrip("ali", _currentValue, date.toString().substring(0,11), time.toString().substring(10,15), description.text, sourceAnnounce.latitude, sourceAnnounce.longitude, destinationAnnounce.latitude, destinationAnnounce.longitude);
                              if(resopnse!.statusCode==200){
                                print("done");

                              }
                            },
                            height: 50,
                            minWidth: 150,
                            color: Color(0xFFd7af0f),
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                color: Color(0xFF5c5c3d),
                                width: 2,
                              ),
                            ),
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
