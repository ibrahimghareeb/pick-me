import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:pick_me/models/connection_manger.dart';
import 'package:pick_me/models/response.dart';
import 'package:pick_me/pages/get_location_map.dart';
import 'package:pick_me/pages/trip_search_result.dart';

class Search_Page extends StatelessWidget {
  const Search_Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SearchBody(),
    );
  }
}

class SearchBody extends StatefulWidget {
  const SearchBody({Key? key}) : super(key: key);

  @override
  _SearchBodyState createState() => _SearchBodyState();
}

class _SearchBodyState extends State<SearchBody> {
  late LatLng destinationLocation = LatLng(0, 0);
  late LatLng sourceLocation = LatLng(0, 0);
  late DateTime date = DateTime.now();
  late TimeOfDay time;

  late TimeOfDay picked;

  late int _currentValue = 1;

  datepicker() {
    showDatePicker(
            helpText: "Date",
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2021),
            builder: (context, child) => Theme(
                data: ThemeData().copyWith(
                  colorScheme: ColorScheme.dark(
                    primary: Color(0xFFd7af0f),
                    onPrimary: Colors.yellow,
                    surface: Color(0xFFd7af0f),
                    onSurface: Colors.black,
                  ),
                  dialogBackgroundColor: Colors.grey.shade400,
                ),
                child: child!),
            lastDate: DateTime(2024))
        .then((value) {
      if (value == null) {
        return;
      }
      setState(() {
        date = value;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    time = TimeOfDay.now();
  }

  Future<Null> selecttime(BuildContext context) async {
    picked = (await showTimePicker(
      context: context,
      initialTime: time,
      builder: (context, child) => Theme(
          data: ThemeData().copyWith(
            colorScheme: ColorScheme.highContrastDark(
              primary: Color(0xFFd7af0f),
              onBackground: Colors.yellow,
              onPrimary: Colors.yellowAccent,
              surface: Colors.grey,
              onSurface: Colors.black,
            ),
          ),
          child: child!),
    ))!;
    if (picked != null) {
      setState(() {
        time = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/app_background.jpg'),
              fit: BoxFit.cover),
        ),
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.1,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.8,
              decoration: BoxDecoration(
                color: Color.fromRGBO(250, 200, 102, 150),
                border: Border.all(
                  color: Color(0xFF5c5c3d),
                  width: 2,
                ),
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              child: Padding(
                padding:  EdgeInsets.only(left: MediaQuery.of(context).size.width*0.02, right: MediaQuery.of(context).size.width*0.02, top: MediaQuery.of(context).size.height*0.04),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //from
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: MediaQuery.of(context).size.height * 0.05,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(214, 214, 194, 100),
                            border:
                                Border.all(color: Color(0xFF5c5c3d), width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Center(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                sourceLocation.toString(),
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                        MaterialButton(
                          onPressed: () async {
                            var result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const GetLocationMap()));
                            setState(() {
                              sourceLocation = result;
                            });
                          },
                          height: MediaQuery.of(context).size.height*0.06,
                          minWidth: MediaQuery.of(context).size.width*0.19,
                          color: Color(0xFFd7af0f),
                          shape: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                              color: Color(0xFF5c5c3d),
                              width: 2,
                            ),
                          ),
                          child: Text(
                            'From',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //to
                    Padding(
                      padding:  EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(214, 214, 194, 100),
                              border: Border.all(
                                  color: Color(0xFF5c5c3d), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  destinationLocation.toString(),
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          MaterialButton(
                            onPressed: () async {
                              var result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const GetLocationMap()));
                              setState(() {
                                destinationLocation = result;
                              });
                            },
                            height: MediaQuery.of(context).size.height*0.06,
                            minWidth: MediaQuery.of(context).size.width*0.19,
                            color: Color(0xFFd7af0f),
                            shape: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                color: Color(0xFF5c5c3d),
                                width: 2,
                              ),
                            ),
                            child: Text(
                              'To',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //Date
                    Padding(
                      padding:  EdgeInsets.only(top: MediaQuery.of(context).size.height*0.04),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.7,
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(214, 214, 194, 100),
                              border: Border.all(
                                  color: Color(0xFF5c5c3d), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                (DateFormat.yMMMd().format(date)).toString(),
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                datepicker();
                              },
                              icon: Icon(
                                Icons.date_range,
                                size: 30,
                              )),
                        ],
                      ),
                    ),
                    //Time
                    Padding(
                      padding:  EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.7,
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(214, 214, 194, 100),
                              border: Border.all(
                                  color: Color(0xFF5c5c3d), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                time.format(context).toString(),
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                selecttime(context);
                              },
                              icon: Icon(
                                Icons.alarm,
                                size: 30,
                              )),
                        ],
                      ),
                    ),
                    //number of people
                    Padding(
                      padding:  EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'number of people :',
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.1,
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(214, 214, 194, 100),
                              border: Border.all(
                                  color: Color(0xFF5c5c3d), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                '$_currentValue',
                                style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.09,
                            height: MediaQuery.of(context).size.height * 0.085,
                            // width: 40,
                            // height: 60,
                            decoration: BoxDecoration(
                              color: Color(0xFFd7af0f),
                              border: Border.all(
                                  color: Color(0xFF5c5c3d), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                            ),
                            child: NumberPicker(
                              value: _currentValue,
                              itemHeight: MediaQuery.of(context).size.height*0.0318,
                              itemWidth: MediaQuery.of(context).size.width*0.0318,
                              minValue: 1,
                              maxValue: 5,
                              textStyle: TextStyle(color: Colors.grey.shade600),
                              selectedTextStyle: TextStyle(color: Colors.black),
                              onChanged: (value) =>
                                  setState(() => _currentValue = value),
                              decoration: BoxDecoration(
                                // borderRadius: BorderRadius.circular(16),
                                border: Border.all(color: Colors.black26),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    //submit
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.08),
                      child: Center(
                        child: MaterialButton(
                          onPressed: () async {
                            Response? response=await getTripFormApi(sourceLocation.latitude, sourceLocation.longitude, destinationLocation.latitude, destinationLocation.longitude,date.toString().substring(0,11),time.toString().substring(10,15));
                            if(response!.statusCode==200){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> HomePageTrips(response: response,)));
                            }
                          },

                          height: MediaQuery.of(context).size.height*0.08,
                          minWidth: MediaQuery.of(context).size.width*0.4,
                          color: Color(0xFFd7af0f),
                          shape: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                              color: Color(0xFF5c5c3d),
                              width: 2,
                            ),
                          ),
                          child: Text(
                            'Search',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
