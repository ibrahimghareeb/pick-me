import 'package:flutter/material.dart';
import 'package:pick_me/models/connection_manger.dart';
import 'package:pick_me/models/response.dart';
import 'package:pick_me/pages/homepage.dart';
import 'package:pick_me/pages/signup.dart';
import 'package:pick_me/widget/button.dart';
import 'package:pick_me/widget/text_field.dart';


class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: HomePageLogin(),
    );
  }
}

class HomePageLogin extends StatefulWidget {
  const HomePageLogin({Key? key}) : super(key: key);


  @override
  _HomePageLoginState createState() => _HomePageLoginState();
}

class _HomePageLoginState extends State<HomePageLogin> {
  final key = GlobalKey<FormState>();
  TextEditingController name = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child:ListView(
            children: [
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.47),
                width: MediaQuery.of(context).size.width ,
                height: MediaQuery.of(context).size.height,

                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/login.jpg'),
                        fit: BoxFit.cover
                    )
                ),
                child: Form(key: key,
                  child: Column(
                    children: [
                      TextFieldName(
                          leftMargin: MediaQuery.of(context).size.width * 0.05,
                          topMargin: MediaQuery.of(context).size.height * 0,
                          rightMargin: MediaQuery.of(context).size.width * 0.05,
                          bottomMargin: MediaQuery.of(context).size.height * 0,
                          controller: name,
                          textInputType: TextInputType.text,
                          fontSizeAll: 15,
                          colorFontStyle: Colors.black,
                          hintText: 'Ahmad',
                          hintSize: 12,
                          hintColor: Colors.grey,
                          labelText: 'UserName',
                          labelSize: 20,
                          labelColor: Colors.black,
                          errorSize: 12,
                          errorColor: Colors.red,
                          borderRadius: 25,
                          borderWidth: 1,
                          borderColor: Colors.grey,
                          enabledBorderRadius: 25,
                          enabledBorderWidth: 1,
                          enabledBorderColor: Colors.grey,
                          focusedBorderRadius: 25,
                          focusedBorderWidth: 1,
                          focusedBorderColor: Colors.grey,
                          errorBorderRadius: 25,
                          errorBorderWidth: 1,
                          errorBorderColor: Colors.red,
                          validate: (value) {
                            if (value!.isEmpty || value == null) {
                              return 'Put your name';
                            } else if (value.length < 3 || value.length > 20) {
                              return 'Your name must be between three and twenty later';
                            } else {
                              return null;
                            }
                          }),
                      TextFieldPassword(
                          leftMargin: MediaQuery.of(context).size.width * 0.05,
                          topMargin: MediaQuery.of(context).size.height * 0.03,
                          rightMargin: MediaQuery.of(context).size.width * 0.05,
                          bottomMargin: MediaQuery.of(context).size.height * 0.03,
                          controller: password,
                          textInputType: TextInputType.text,
                          fontSizeAll: 15,
                          colorFontStyle: Colors.black,
                          hintText: 'as!@123',
                          hintSize: 12,
                          hintColor: Colors.grey,
                          labelText: 'Password',
                          labelSize: 20,
                          labelColor: Colors.black,
                          errorSize: 12,
                          errorColor: Colors.red,
                          borderRadius: 25,
                          borderWidth: 1,
                          borderColor: Colors.grey,
                          enabledBorderRadius: 25,
                          enabledBorderWidth: 1,
                          enabledBorderColor: Colors.grey,
                          focusedBorderRadius: 25,
                          focusedBorderWidth: 1,
                          focusedBorderColor: Colors.grey,
                          errorBorderRadius: 25,
                          errorBorderWidth: 1,
                          errorBorderColor: Colors.red,
                          validate: (value) {
                            if (value!.isEmpty || value == null) {
                              return 'Your PASSWORD ';
                            } else if (value.length < 8) {
                              return 'Your password must be more than eight characters';
                            } else {
                              return null;
                            }
                          }),
                      ButtonFormWidget(
                          minWidth: MediaQuery.of(context).size.width * 0.6,
                          borderRadius: 10,
                          borderColor: Colors.black,
                          borderWidth: 1,
                          buttonText: 'Login',
                          textColor: Colors.black,
                          textSize: 15,
                          onPressed: () async {
                            bool value=key.currentState!.validate();
                            if(value){
                              Response? response= await logIn(name.text, password.text);
                              if (response!.statusCode==200){
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const HomePage()));
                              }
                              return null;
                            }
                          }),
                      TextButton(child:const Text('Forgot Password !!',style:  TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),),
                        onPressed: (){
                          // TODO: FUNCTION for forgot password
                        },


                      ),
                      ButtonCreateAccount(
                          minWidth: MediaQuery.of(context).size.width * 0.4,
                          borderRadius: 2,
                          borderColor: Colors.black,
                          borderWidth: 1,
                          buttonText: 'Create Account',
                          textColor: Colors.white,
                          textSize: 15,
                          onPressed: () {

                            Navigator.push(context, MaterialPageRoute(builder: (context)=>const SignupPage()));

                          }
                      ),




                    ],)
                  ,),
              ),]
        ) );
  }
}

