import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pick_me/models/connection_manger.dart';
import 'package:pick_me/models/response.dart';
import 'package:pick_me/models/trip_model.dart';
import 'package:pick_me/pages/homepage.dart';
import 'package:pick_me/widget/detail.dart';
import 'package:flutter/material.dart';
import 'dart:convert';



class HomePageTrips extends StatefulWidget {
  Response response;

  HomePageTrips({required this.response}) ;

  @override
  _HomePageTripsState createState() => _HomePageTripsState();
}

class _HomePageTripsState extends State<HomePageTrips> {
  late List <TripModel> tripModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tripModel=widget.response.body;
    print(tripModel.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.03,bottom: MediaQuery.of(context).size.height*0.1),
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/app_background.jpg'),
                    fit: BoxFit.cover,
                  )
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: tripModel.map((e) => DetailWidget(advertiserName: e.username, dateAndTime: e.date, numberOfPeople: e.passengersCount.toString(), descriptionText: e.description, onPressed: () async {
                    Response? response = await reqToJoin(e.username, e.id.toString());
                    setState(() {

                      e.passengersCount--;

                      if(response!.statusCode==200){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> HomePage()));
                      }
                    });
                  }, sourceMap: LatLng(e.sourceLatitude,e.sourceLongitude), destinationMap: LatLng(e.destinationLatitude,e.destinationLongitude))).toList(),

                    //DetailWidget(advertiserName: 'ali', dateAndTime: '1/2/2021', numberOfPeople: '8',),
                    // DetailWidget(advertiserName: 'ali', dateAndTime: '1/2/2021', numberOfPeople: '4', descriptionText: 'rrrrrr', onPressed: () {  }, dropdownButtonValue: 'One', listOfNamesDropdownButton: ['One','Tow','Three'], destinationMap: LatLng(35.585854,35.474858585),sourceMap: LatLng(35.77777,35.88889),),
                    // DetailWidget(advertiserName: 'ali', dateAndTime: '1/2/2021', numberOfPeople: '4', descriptionText: 'rrrrrr', onPressed: () {  }, dropdownButtonValue: 'One', listOfNamesDropdownButton: ['One','Tow','Three'], destinationMap: LatLng(35.585854,35.474858585),sourceMap: LatLng(35.77777,35.88889),)



                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

