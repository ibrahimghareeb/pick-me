import 'package:flutter/material.dart';
import 'package:pick_me/pages/announcing.dart';
import 'package:pick_me/pages/search.dart';

class HomePage extends StatelessWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold( 
     body:Homebody() ,
    );
  }
}

class Homebody extends StatefulWidget {
  const Homebody({ Key? key }) : super(key: key);

  @override
  _HomebodyState createState() => _HomebodyState();
}

class _HomebodyState extends State<Homebody> {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height:MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image:AssetImage('assets/images/app_background.jpg'),fit: BoxFit.cover ),
      ),
      child: Column(
        children: [
         SizedBox(
           height:MediaQuery.of(context).size.height*0.25,
         ),
         MaterialButton(
           onPressed: (){
             Navigator.push(context,MaterialPageRoute(builder: (context)=>const Announcing_Page()),);
           },
           height:MediaQuery.of(context).size.height*0.2,
           minWidth: MediaQuery.of(context).size.width*0.9,
           color: Color(0xFFd7af0f),
           shape: OutlineInputBorder(
             borderRadius: BorderRadius.all(Radius.circular(30)),
             borderSide: BorderSide(color: Color(0xFF5c5c3d),width: 3,),
           ),
           child: Text('Announcing a Trip',
           style: TextStyle(
             fontSize: 25,
             fontWeight: FontWeight.bold,
           ),),
         ),
         SizedBox(
           height:MediaQuery.of(context).size.height*0.15,
         ),
         MaterialButton(
           onPressed: (){
             Navigator.push(context,MaterialPageRoute(builder: (context)=>const Search_Page()),);
           },
           height:MediaQuery.of(context).size.height*0.2,
           minWidth: MediaQuery.of(context).size.width*0.9,
           color: Color(0xFFd7af0f),
           shape: OutlineInputBorder(
             borderRadius: BorderRadius.all(Radius.circular(30)),
             borderSide: BorderSide(color: Color(0xFF5c5c3d),width: 3,),
           ),
           child: Text('Search for a Trip',
           style: TextStyle(
             fontSize: 25,
             fontWeight: FontWeight.bold,
           ),),
         ),
      ],
      ),
    );
  }
}