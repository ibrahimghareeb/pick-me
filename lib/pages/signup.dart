import 'package:flutter/material.dart';
import 'package:pick_me/models/connection_manger.dart';
import 'package:pick_me/models/response.dart';
import 'package:pick_me/pages/login.dart';
import 'package:pick_me/widget/button.dart';
import 'package:pick_me/widget/text_field.dart';

import 'homepage.dart';

class SignupPage extends StatelessWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: HomePageSignIn(),
    );
  }
}

class HomePageSignIn extends StatefulWidget {
  const HomePageSignIn({Key? key}) : super(key: key);

  @override
  _HomePageSignInState createState() => _HomePageSignInState();
}

class _HomePageSignInState extends State<HomePageSignIn> {
  final key = GlobalKey<FormState>();
  TextEditingController name = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController rePassword = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: ListView(
          children: [
            Container(

                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.47),
                width: MediaQuery.of(context).size.width,

                decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/signup.jpg'),
                      fit: BoxFit.cover,
                    )),
                child: Form(
                    key: key,
                    child: Column(
                      children: [
                        TextFieldName(
                            leftMargin: MediaQuery.of(context).size.width * 0.05,
                            topMargin: MediaQuery.of(context).size.height * 0,
                            rightMargin: MediaQuery.of(context).size.width * 0.05,
                            bottomMargin: MediaQuery.of(context).size.height * 0,
                            controller: name,
                            textInputType: TextInputType.text,
                            fontSizeAll: 15,
                            colorFontStyle: Colors.black,
                            hintText: 'Ahmad',
                            hintSize: 12,
                            hintColor: Colors.grey,
                            labelText: 'UserName',
                            labelSize: 20,
                            labelColor: Colors.black,
                            errorSize: 12,
                            errorColor: Colors.red,
                            borderRadius: 25,
                            borderWidth: 1,
                            borderColor: Colors.grey,
                            enabledBorderRadius: 25,
                            enabledBorderWidth: 1,
                            enabledBorderColor: Colors.grey,
                            focusedBorderRadius: 25,
                            focusedBorderWidth: 1,
                            focusedBorderColor: Colors.grey,
                            errorBorderRadius: 25,
                            errorBorderWidth: 1,
                            errorBorderColor: Colors.red,
                            validate: (value) {
                              if (value!.isEmpty || value == null) {
                                return 'Put your name';
                              } else if (value.length < 3 || value.length > 20) {
                                return 'Your name must be between three and twenty letters';
                              } else {
                                return null;
                              }
                            }),
                        TextFieldName(
                            leftMargin: MediaQuery.of(context).size.width * 0.05,
                            topMargin: MediaQuery.of(context).size.height * 0.03,
                            rightMargin: MediaQuery.of(context).size.width * 0.05,
                            bottomMargin: MediaQuery.of(context).size.height * 0,
                            controller: phoneNumber,
                            textInputType: TextInputType.number,
                            fontSizeAll: 15,
                            colorFontStyle: Colors.black,
                            hintText: '0949xxxxxx',
                            hintSize: 12,
                            hintColor: Colors.grey,
                            labelText: 'PhoneNumber',
                            labelSize: 20,
                            labelColor: Colors.black,
                            errorSize: 12,
                            errorColor: Colors.red,
                            borderRadius: 25,
                            borderWidth: 1,
                            borderColor: Colors.grey,
                            enabledBorderRadius: 25,
                            enabledBorderWidth: 1,
                            enabledBorderColor: Colors.grey,
                            focusedBorderRadius: 25,
                            focusedBorderWidth: 1,
                            focusedBorderColor: Colors.grey,
                            errorBorderRadius: 25,
                            errorBorderWidth: 1,
                            errorBorderColor: Colors.red,
                            validate: (value) {
                              if (value!.isEmpty || value == null) {
                                return 'Put your phone number';
                              } else if (value.length < 10 || value.length > 10) {
                                return 'Your phone number must be ten number';
                              } else if(!value.startsWith('09')){
                                return 'Your number must start with 09';

                              }else {
                                return null;
                              }
                            }),
                        TextFieldPassword(
                            leftMargin: MediaQuery.of(context).size.width * 0.05,
                            topMargin: MediaQuery.of(context).size.height * 0.03,
                            rightMargin: MediaQuery.of(context).size.width * 0.05,
                            bottomMargin: MediaQuery.of(context).size.height * 0,
                            controller: password,
                            textInputType: TextInputType.text,
                            fontSizeAll: 15,
                            colorFontStyle: Colors.black,
                            hintText: 'as!@123',
                            hintSize: 12,
                            hintColor: Colors.grey,
                            labelText: 'Password',
                            labelSize: 20,
                            labelColor: Colors.black,
                            errorSize: 12,
                            errorColor: Colors.red,
                            borderRadius: 25,
                            borderWidth: 1,
                            borderColor: Colors.grey,
                            enabledBorderRadius: 25,
                            enabledBorderWidth: 1,
                            enabledBorderColor: Colors.grey,
                            focusedBorderRadius: 25,
                            focusedBorderWidth: 1,
                            focusedBorderColor: Colors.grey,
                            errorBorderRadius: 25,
                            errorBorderWidth: 1,
                            errorBorderColor: Colors.red,
                            validate: (value) {
                              if (value!.isEmpty || value == null) {
                                return 'Input valid PASSWORD';
                              } else if (value.length < 8) {
                                return 'Your password must be more than eight characters';
                              } else {
                                return null;
                              }
                            }),
                        TextFieldPassword(
                            leftMargin: MediaQuery.of(context).size.width * 0.05,
                            topMargin: MediaQuery.of(context).size.height * 0.03,
                            rightMargin: MediaQuery.of(context).size.width * 0.05,
                            bottomMargin: MediaQuery.of(context).size.height * 0,
                            controller: rePassword,
                            textInputType: TextInputType.text,
                            fontSizeAll: 15,
                            colorFontStyle: Colors.black,
                            hintText: '*****',
                            hintSize: 12,
                            hintColor: Colors.grey,
                            labelText: 'Retype-Password',
                            labelSize: 20,
                            labelColor: Colors.black,
                            errorSize: 12,
                            errorColor: Colors.red,
                            borderRadius: 25,
                            borderWidth: 1,
                            borderColor: Colors.grey,
                            enabledBorderRadius: 25,
                            enabledBorderWidth: 1,
                            enabledBorderColor: Colors.grey,
                            focusedBorderRadius: 25,
                            focusedBorderWidth: 1,
                            focusedBorderColor: Colors.grey,
                            errorBorderRadius: 25,
                            errorBorderWidth: 1,
                            errorBorderColor: Colors.red,
                            validate: (value) {
                              if (value != password.text) {
                                return 'Your PASSWORD is not similar';
                              } else {
                                return null;
                              }
                            }),
                        ButtonFormWidget(
                            minWidth: MediaQuery.of(context).size.width * 0.6,
                            borderRadius: 10,
                            borderColor: Colors.black,
                            borderWidth: 1,
                            buttonText: 'Register Now',
                            textColor: Colors.black,
                            textSize: 15,
                            onPressed: () async {
                              bool value=key.currentState!.validate();
                              if(value){
                                Response? response= await registerUser(name.text, password.text,phoneNumber.text);
                                if (response!.statusCode==200){
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const HomePage()));
                                }
                                print('ok');
                                return null;
                              }
                            }),
                        // Container for login
                        Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.2),
                          child: Row(
                            children: [
                              const Text('Already have an account?',style: TextStyle(
                                fontSize: 15,
                              ),),
                              TextButton(child: Text('login',style: TextStyle(
                                  color: Colors.yellow[900],
                                  fontSize: 15
                              ),),
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>const LoginPage()));
                                },
                              )
                            ],
                          ),
                        ),


                      ],
                    ))),
          ],
        ));
  }
}

//ButtonFormWidget(leftMargin: MediaQuery.of(context).size.width*0.1, topMargin: MediaQuery.of(context).size.height*0.015, rightMargin: MediaQuery.of(context).size.width*0.1, bottomMargin: MediaQuery.of(context).size.height*0, backgroundColor: Colors.deepOrange, borderRadius: 2, borderColor: Colors.grey, borderWidth: 1, buttonText: 'Register Now', textColor: Colors.black, textSize: 12, onPressed: (){

//}),
