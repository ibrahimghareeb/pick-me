import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:pick_me/models/response.dart';
import 'package:pick_me/models/trip_model.dart';
import 'package:pick_me/models/user_model.dart';

String baseUrl='http://192.168.0.126:3000';
Future<Response?> getTripFormApi(double sourceLatitudem, double sourceLongitude,
    double destinationLatitude, double destinationLongitude,String date,String time) async {
  var url = Uri.parse(
      '$baseUrl/showTrips?source_latitude=$sourceLatitudem&source_longitude=$sourceLongitude&destination_latitude=$destinationLatitude&destination_longitude=$destinationLongitude&date=$date&time=$time');
  http.Response response = await http.get(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  });
  var data = json.decode(response.body);
  if (response.statusCode == 200) {
    List list = (data as List).map((e) => TripModel.fromJson(e)).toList();
    return Response(statusCode: 200, body: list);
  } else if (response.statusCode == 500) {
    return Response(statusCode: 500, body: "no internet");
  }
}

Future<Response?> registerUser(
    String name, String password, String number) async {
  var url = Uri.parse('$baseUrl/signup');
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  }, body: {
    "username": name,
    "password": password,
    "phone": number
  });
  if (response.statusCode == 200) {
    return Response(statusCode: 200, body: "register successfully");
  } else if (response.statusCode == 401) {
    return Response(statusCode: 401, body: "null");
  }
}

Future<Response?> logIn(String name, String password) async {
  var url = Uri.parse('$baseUrl/login');
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  }, body: {
    "username": name,
    "password": password,
  });
  if (response.statusCode == 200) {
    return Response(statusCode: 200, body: "login successfully");
  } else if (response.statusCode == 401) {
    return Response(statusCode: 401, body: "faild");
  }
}

Future<Response?> addTrip(
    String username,
    int passengersCount,
    String date,
    String time,
    String description,
    double sourceLatitude,
    double sourceLongitude,
    double destinationLatitude,
    double destinationLongitude) async {
  var url = Uri.parse('$baseUrl/addTrip');
  String trip = json.encode({
    "username": username,
    "passengers": passengersCount,
    "date": date,
    "time": time,
    "description": description,
    "source_latitude": sourceLatitude,
    "source_longitude": sourceLongitude,
    "destination_latitude": destinationLatitude,
    "destination_longitude": destinationLongitude,
  });
  print(trip);
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'applicatoin/json'
  }, body: {
    "username": username,
    "passengers": passengersCount.toString(),
    "date": date,
    "time": time,
    "description": description,
    "source_latitude": sourceLatitude.toString(),
    "source_longitude": sourceLongitude.toString(),
    "destination_latitude": destinationLatitude.toString(),
    "destination_longitude": destinationLongitude.toString(),
  });
  if (response.statusCode == 200) {
    return Response(statusCode: 200, body: "trip successfully added");
  } else if (response.statusCode == 401) {
    return Response(statusCode: 401, body: "faild");
  }
}

Future<Response?> reqToJoin(String name, String tripId) async {
  var url = Uri.parse('$baseUrl/requestToJoin');
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'applicatoin/json'
  }, body: {
    "trip_id": tripId,
    "username": name,
  });
  if (response.statusCode == 200) {
    return Response(statusCode: 200, body: "request join successfully");
  } else if (response.statusCode == 401) {
    return Response(statusCode: 401, body: "faild");
  }
}

Future<Response?> showTripPassengers(String tripId) async {
  var url = Uri.parse(
      'http://192.168.0.110:3000/showTripPassengers?trip_id=' + tripId);
  http.Response response = await http.get(url, headers: {
    'content_type': 'application/json',
    'accept': 'applicatoin/json'
  });
  var data = json.decode(response.body);
  if (response.statusCode == 200) {
    List list = (data as List).map((e) => UserModel.fromJson(e)).toList();
    return Response(statusCode: 200, body: list);
  } else if (response.statusCode == 500) {
    return Response(statusCode: 500, body: "no internet");
  }
}
