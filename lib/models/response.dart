class Response {
  int statusCode;
  dynamic body;
  Response({required this.statusCode, this.body});
}
