class TripModel {
  late int id;
  late String username;
  late int sourceId;
  late int destinationId;
  late int passengersCount;
  late String date;
  late String time;
  late String description;
  late double sourceLatitude;
  late double sourceLongitude;
  late double destinationLatitude;
  late double destinationLongitude;

  TripModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    sourceId = json['source_id'];
    destinationId = json['destination_id'];
    passengersCount = json['passengers_count'];
    date = json['date'];
    time = json['time'];
    description = json['description'];
    sourceLatitude = json['source_latitude'];
    sourceLongitude = json['source_longitude'];
    destinationLatitude = json['destination_latitude'];
    destinationLongitude = json['destination_longitude'];
  }
}
